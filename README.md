# Static Song Books on GitLab Pages

A published example of this project can be found at...

> https://static-song-books.gitlab.io/example/

## How To Use

Fork this project in GitLab.

Modify the songs.csv file ensuring there are at least two columns, delimited by the ; character.

1. `Artist`
2. `Title`

Go to CI/CD Pipelines and launch the Pipeline.

## Further Modifications

Read up on [Ansible](https://docs.ansible.com/ansible/latest/index.html) and [MkDocs](https://www.mkdocs.org/).

See [mkdocs-material](https://squidfunk.github.io/mkdocs-material/getting-started/) theme documentation for more options.

## Custom Domain Name

Follow the instructions [here](https://dev.to/stmcallister/assigning-a-google-domain-to-gitlab-pages-22ap) to line up your gitlab page with your domain.